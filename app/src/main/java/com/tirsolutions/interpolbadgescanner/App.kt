package com.tirsolutions.interpolbadgescanner

import android.app.Application
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.imagepipeline.core.ImagePipelineConfig
import com.tirsolutions.interpolbadgescanner.data.source.local.SettingDL
import com.tirsolutions.interpolbadgescanner.data.source.local.TransactionDL
import com.tirsolutions.interpolbadgescanner.data.source.local.UserDL

class App: Application() {

    override fun onCreate() {
        super.onCreate()

        val config = ImagePipelineConfig.newBuilder(this)
            .setDownsampleEnabled(true)
            .build()

        Fresco.initialize(this, config)

        UserDL.init(this)
        SettingDL.init(this)
        TransactionDL.init(this)
    }
}
