package com.tirsolutions.interpolbadgescanner.data.source.local

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import com.tirsolutions.interpolbadgescanner.data.Setting
import com.tirsolutions.interpolbadgescanner.data.Transaction
import com.tirsolutions.interpolbadgescanner.data.User
import com.tirsolutions.interpolbadgescanner.utils.DATABASE_NAME
import com.tirsolutions.interpolbadgescanner.utils.DATABASE_VERSION
import com.tirsolutions.interpolbadgescanner.data.source.local.UserDao

@Database(entities = [User::class, Transaction::class, Setting::class], version = DATABASE_VERSION)
abstract class AppDatabase: RoomDatabase() {

    abstract fun getUserDao(): UserDao
    abstract fun getTransactionDao(): TransactionDao
    abstract fun getSettingDao(): SettingDao

    companion object {
        private var INSTANCE: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase? {
            if (INSTANCE == null) {
                synchronized(AppDatabase::class) {
                    INSTANCE = Room.databaseBuilder(context, AppDatabase::class.java, DATABASE_NAME)
                        .allowMainThreadQueries()
                        .fallbackToDestructiveMigration()
                        .build()
                }
            }

            return INSTANCE
        }
    }
}
