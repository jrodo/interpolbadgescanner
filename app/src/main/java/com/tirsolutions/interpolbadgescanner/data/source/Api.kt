package com.tirsolutions.interpolbadgescanner.data.source

interface Api<T> {
    fun create(baseUrl: String): T
}
