package com.tirsolutions.interpolbadgescanner.data.source.remote

import com.tirsolutions.interpolbadgescanner.data.source.Api
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class InterpolBadgeScannerApi: Api<InterpolBadgeScannerService> {

    override fun create(baseUrl: String): InterpolBadgeScannerService {
        val client = OkHttpClient().newBuilder()
            .build()
        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build().create(InterpolBadgeScannerService::class.java)
    }
}
