package com.tirsolutions.interpolbadgescanner.data.source.local

import android.content.Context
import com.tirsolutions.interpolbadgescanner.data.User

class UserDL(context: Context): AppCache<User> {

    private val dao = AppDatabase.getInstance(context)!!.getUserDao()

    override fun get(id: Int): User {
        return dao.getUser(id)
    }

    override fun getAll(): List<User> {
        return dao.getAllUsers()
    }

    override fun getLast(): User {
        return dao.getLastUser()
    }

    override fun save(data: User) {
        dao.saveUser(data)
    }

    override fun saveAll(dataList: List<User>) {
        dataList.forEach { data ->
            dao.saveUser(data)
        }
    }

    override fun remove(data: User) {
        dao.removeUser(data)
    }

    override fun clear() {
        dao.clearUser()
    }

    override fun isEmpty(): Boolean {
        return dao.getAllUsers().isEmpty()
    }

    fun getByCredentials(username: String, password: String): User? {
        return dao.getUserByCredentials(username, password)
    }

    fun  getLoggedInUser(isLoggedIn: Boolean): User {
        return dao.getLoggedInUser(isLoggedIn)
    }

    fun updateUserIsLoggedIn(isLoggedIn: Boolean, id: Int) {
        dao.updateUserIsLoggedIn(isLoggedIn, id)
    }

    companion object {
        private var INSTANCE: UserDL? = null

        fun init(context: Context) {
            INSTANCE = UserDL(context)
        }

        fun getInstance(): UserDL {
            return INSTANCE!!
        }
    }
}
