package com.tirsolutions.interpolbadgescanner.data.source.local

interface AppCache<T> {
    fun get(id: Int): T
    fun getAll(): List<T>
    fun getLast(): T
    fun save(data: T)
    fun saveAll(dataList: List<T>)
    fun remove(data: T)
    fun clear()
    fun isEmpty(): Boolean
}
