package com.tirsolutions.interpolbadgescanner.data

data class ApiResponse<T> (
    var code: Int = 0,
    var status: String? = null,
    var message: String? = null,
    var results: T? = null
)
