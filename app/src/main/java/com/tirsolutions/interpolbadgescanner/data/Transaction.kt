package com.tirsolutions.interpolbadgescanner.data

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import com.tirsolutions.interpolbadgescanner.utils.TABLE_NAME_TRANSACTIONS

@Entity(tableName = TABLE_NAME_TRANSACTIONS)
data class Transaction (
    @SerializedName("visitor_id")
    @ColumnInfo(name = "visitor_id")
    var visitorId: String,

    @SerializedName("log_type")
    @ColumnInfo(name = "log_type")
    var logType: String,

    @SerializedName("created_at")
    @ColumnInfo(name = "created_at")
    var createdAt: String,

    @SerializedName("updated_at")
    @ColumnInfo(name = "updated_at")
    var updatedAt: String,

    @ColumnInfo(name = "is_uploaded")
    var isUploaded: Boolean,

    @ColumnInfo(name = "user")
    var userId: Int
) {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id: Int? = null
}
