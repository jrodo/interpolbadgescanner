package com.tirsolutions.interpolbadgescanner.data.source.local

import android.content.Context
import com.tirsolutions.interpolbadgescanner.data.Transaction

class TransactionDL(context: Context): AppCache<Transaction> {

    private val dao = AppDatabase.getInstance(context)!!.getTransactionDao()

    override fun get(id: Int): Transaction {
        return dao.getTransaction(id)
    }

    override fun getAll(): List<Transaction> {
        return dao.getAllTransactions()
    }

    override fun getLast(): Transaction {
        return dao.getLastTransaction()
    }

    override fun save(data: Transaction) {
        dao.saveTransaction(data)
    }

    override fun saveAll(dataList: List<Transaction>) {
        dataList.forEach { data ->
            dao.saveTransaction(data)
        }
    }

    override fun remove(data: Transaction) {
        dao.removeTransaction(data)
    }

    override fun clear() {
        dao.clearTransaction()
    }

    override fun isEmpty(): Boolean {
        return dao.getAllTransactions().isEmpty()
    }

    fun updateIsUploaded(isUploaded: Boolean, id: Int) {
        dao.updateIsUploaded(isUploaded, id)
    }

    fun getAllTransactionByUser(user: Int): List<Transaction> {
        return dao.getAllTransactionByUser(user)
    }

    fun getAllLocalTransaction(isUploaded: Boolean): List<Transaction> {
        return dao.getAllLocalTransactions(isUploaded)
    }

    fun getAllLocalTransactionByUser(isUploaded: Boolean, user: Int): List<Transaction> {
        return dao.getAllLocalTransactionsByUser(isUploaded, user)
    }

    companion object {
        private var INSTANCE: TransactionDL? = null

        fun init(context: Context) {
            INSTANCE = TransactionDL(context)
        }

        fun getInstance(): TransactionDL {
            return INSTANCE!!
        }
    }
}
