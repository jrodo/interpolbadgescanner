package com.tirsolutions.interpolbadgescanner.data.source.remote

import com.tirsolutions.interpolbadgescanner.data.ApiResponse
import com.tirsolutions.interpolbadgescanner.data.GenericApiResponse
import com.tirsolutions.interpolbadgescanner.data.Transaction
import com.tirsolutions.interpolbadgescanner.data.User
import com.tirsolutions.interpolbadgescanner.utils.PATH_LOGIN
import com.tirsolutions.interpolbadgescanner.utils.PATH_TRANSACTION
import com.tirsolutions.interpolbadgescanner.utils.PATH_TRANSACTION_BULK
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.*

interface InterpolBadgeScannerService {

    @POST(PATH_LOGIN)
    @FormUrlEncoded
    fun login(
        @Field("username") username: String,
        @Field("password") password: String
    ): Observable<Response<ApiResponse<User>>>

    @POST(PATH_TRANSACTION_BULK)
    fun transactionBulk(
        @Body logs: ArrayList<Transaction>
    ): Observable<Response<GenericApiResponse>>
}
