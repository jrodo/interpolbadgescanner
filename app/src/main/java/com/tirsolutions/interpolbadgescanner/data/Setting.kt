package com.tirsolutions.interpolbadgescanner.data

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.tirsolutions.interpolbadgescanner.utils.TABLE_NAME_SETTINGS

@Entity(tableName = TABLE_NAME_SETTINGS)
data class Setting(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id: Int? = null,

    @ColumnInfo(name = "base_url")
    var baseUrl: String
)
