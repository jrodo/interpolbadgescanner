package com.tirsolutions.interpolbadgescanner.data

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import com.tirsolutions.interpolbadgescanner.utils.TABLE_NAME_USERS

@Entity(tableName = TABLE_NAME_USERS)
data class User (
    @SerializedName("first_name")
    @ColumnInfo(name = "first_name")
    var firstName: String?,

    @SerializedName("last_name")
    @ColumnInfo(name = "last_name")
    var lastName: String?,

    @ColumnInfo(name = "username")
    var username: String,

    @ColumnInfo(name = "password")
    var password: String,

    @ColumnInfo(name = "is_logged_in")
    var isLoggedIn: Boolean
) {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id: Int? = null
}
