package com.tirsolutions.interpolbadgescanner.data.source.local

import android.arch.persistence.room.*
import com.tirsolutions.interpolbadgescanner.data.Setting

@Dao
interface SettingDao {

    @Query("SELECT * FROM settings")
    fun getAllSetting(): List<Setting>

    @Query("SELECT * FROM settings WHERE id=:id")
    fun getSetting(id: Int): Setting

    @Query("SELECT * FROM settings ORDER BY id DESC")
    fun getLastSetting(): Setting

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveSetting(setting: Setting)

    @Delete
    fun removeSetting(setting: Setting)

    @Query("DELETE FROM settings")
    fun clearSetting()
}
