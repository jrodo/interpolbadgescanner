package com.tirsolutions.interpolbadgescanner.data.source.local

import android.arch.persistence.room.*
import com.tirsolutions.interpolbadgescanner.data.Transaction

@Dao
interface TransactionDao {

    @Query("SELECT * FROM transactions")
    fun getAllTransactions(): List<Transaction>

    @Query("SELECT * FROM transactions WHERE user=:user")
    fun getAllTransactionByUser(user: Int): List<Transaction>

    @Query("SELECT * FROM transactions WHERE id=:id")
    fun getTransaction(id: Int): Transaction

    @Query("SELECT * FROM transactions WHERE is_uploaded=:isUploaded")
    fun getAllLocalTransactions(isUploaded: Boolean): List<Transaction>

    @Query("SELECT * FROM transactions WHERE is_uploaded=:isUploaded AND user=:user")
    fun getAllLocalTransactionsByUser(isUploaded: Boolean, user: Int): List<Transaction>

    @Query("SELECT * FROM transactions ORDER BY id DESC")
    fun getLastTransaction(): Transaction

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveTransaction(lead: Transaction)

    @Query("UPDATE transactions SET is_uploaded=:isUploaded WHERE id=:id")
    fun updateIsUploaded(isUploaded: Boolean, id: Int)

    @Delete
    fun removeTransaction(lead: Transaction)

    @Query("DELETE FROM transactions")
    fun clearTransaction()
}
