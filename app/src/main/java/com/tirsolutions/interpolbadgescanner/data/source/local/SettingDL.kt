package com.tirsolutions.interpolbadgescanner.data.source.local

import android.content.Context
import com.tirsolutions.interpolbadgescanner.data.Setting

class SettingDL(context: Context): AppCache<Setting> {

    private val dao = AppDatabase.getInstance(context)!!.getSettingDao()

    override fun get(id: Int): Setting {
        return dao.getSetting(id)
    }

    override fun getAll(): List<Setting> {
        return dao.getAllSetting()
    }

    override fun getLast(): Setting {
        return dao.getLastSetting()
    }

    override fun save(data: Setting) {
        dao.saveSetting(data)
    }

    override fun saveAll(dataList: List<Setting>) {
        dataList.forEach { data ->
            dao.saveSetting(data)
        }
    }

    override fun remove(data: Setting) {
        dao.removeSetting(data)
    }

    override fun clear() {
        dao.clearSetting()
    }

    override fun isEmpty(): Boolean {
        return dao.getAllSetting().isEmpty()
    }

    companion object {
        private var INSTANCE: SettingDL? = null

        fun init(context: Context) {
            INSTANCE = SettingDL(context)
        }

        fun getInstance(): SettingDL {
            return INSTANCE!!
        }
    }
}
