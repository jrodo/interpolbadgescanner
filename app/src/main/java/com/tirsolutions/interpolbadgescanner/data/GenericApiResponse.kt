package com.tirsolutions.interpolbadgescanner.data

data class GenericApiResponse (
    var code: Int = 0,
    var status: String? = null,
    var message: String? = null
)
