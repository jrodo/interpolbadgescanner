package com.tirsolutions.interpolbadgescanner.data.source.local

import android.arch.persistence.room.*
import com.tirsolutions.interpolbadgescanner.data.User

@Dao
interface UserDao {

    @Query("SELECT * FROM users")
    fun getAllUsers(): List<User>

    @Query("SELECT * FROM users WHERE id=:id")
    fun getUser(id: Int): User

    @Query("SELECT * FROM users ORDER BY id DESC")
    fun getLastUser(): User

    @Query("SELECT * FROM users WHERE is_logged_in=:isLoggedIn")
    fun getLoggedInUser(isLoggedIn: Boolean): User

    @Query("SELECT * FROM users WHERE username=:username AND password=:password")
    fun getUserByCredentials(username: String, password: String): User?

    @Query("UPDATE users SET is_logged_in=:isLoggedIn WHERE id=:id")
    fun updateUserIsLoggedIn(isLoggedIn: Boolean, id: Int)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveUser(user: User)

    @Delete
    fun removeUser(user: User)

    @Query("DELETE FROM users")
    fun clearUser()
}
