package com.tirsolutions.interpolbadgescanner

interface BasePresenter<V> {

    fun attachView(view: V)

    fun detachView()
}
