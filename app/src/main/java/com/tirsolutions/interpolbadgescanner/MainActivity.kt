package com.tirsolutions.interpolbadgescanner

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.tirsolutions.interpolbadgescanner.presentation.login.LoginFragment
import com.tirsolutions.interpolbadgescanner.presentation.login.LoginPresenter
import com.tirsolutions.interpolbadgescanner.presentation.qrcodescanner.QrcodeScannerFragment
import com.tirsolutions.interpolbadgescanner.utils.FRAGMENT_TAG_LOGIN
import com.tirsolutions.interpolbadgescanner.utils.FRAGMENT_TAG_QRCODESCANNER
import com.tirsolutions.interpolbadgescanner.utils.FragmentHelper

class MainActivity : AppCompatActivity() {

    private val loginPresenter = LoginPresenter()

    override fun onCreate(savedInstanceState: Bundle?) {
        //call splash screen before onCreate
        setTheme(R.style.SplashTheme)
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        if (savedInstanceState == null) {
            FragmentHelper.replaceFragment(this, LoginFragment(), FRAGMENT_TAG_LOGIN)
//            FragmentHelper.replaceFragment(this, QrcodeScannerFragment(), FRAGMENT_TAG_QRCODESCANNER)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        loginPresenter.clearLoggedInUser()
    }
}
