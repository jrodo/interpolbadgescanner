package com.tirsolutions.interpolbadgescanner.presentation.setting

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.widget.Toast
import com.tirsolutions.interpolbadgescanner.R
import com.tirsolutions.interpolbadgescanner.presentation.login.LoginFragment
import com.tirsolutions.interpolbadgescanner.utils.BASE_URL
import com.tirsolutions.interpolbadgescanner.utils.FRAGMENT_TAG_LOGIN
import com.tirsolutions.interpolbadgescanner.utils.FragmentHelper
import kotlinx.android.synthetic.main.fragment_setting.*
import android.text.Editable
import android.text.Selection
import android.text.TextWatcher
import android.view.*

class SettingFragment: Fragment(), SettingContract.View {

    private val presenter = SettingPresenter()

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        presenter.attachView(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_setting, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity!!.title = "Settings"

        if (presenter.hasSavedSetting()) {
            editTextBaseUrl.setText(presenter.getSetting().baseUrl)
        } else {
            editTextBaseUrl.setText("http://devapi.interpol.oneberrysystem.com")
        }

        setTextWatcher()

        var clickCount = 0
        appLogo.setOnClickListener {
            clickCount += 1

            if (clickCount == 3) {
                editTextBaseUrl.setText(BASE_URL)
            }
        }

        btnSave.setOnClickListener {
            if (!hasEmptyFields()) {
                if (isValidUrl()) {
                    presenter.onSaveClick(editTextBaseUrl.text.toString())
                } else {
                    editTextBaseUrl.setError("Invalid url. Add http:// as prefix")
//                    showToastMessage("Invalid url. Add http:// as string prefix")
                }
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater!!.inflate(R.menu.settings_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            R.id.nav_wipe -> {
                showAlertDialog("Are you sure you want to wipe users and transactions data?")
            }
        }
        return false
    }

    override fun onDetach() {
        super.onDetach()
        presenter.detachView()
    }

    override fun showProgressDialog() {

    }

    override fun hideProgressDialog() {

    }

    override fun showToastMessage(message: String) {
        Toast.makeText(activity!!, message, Toast.LENGTH_SHORT).show()
    }

    override fun showLoginFragment() {
        FragmentHelper.replaceFragment(activity!!, LoginFragment(), FRAGMENT_TAG_LOGIN)
    }

    override fun showAlertDialog(message: String) {
        val builder = AlertDialog.Builder(activity)
        builder.setMessage(message)
            .setCancelable(false)
            .setPositiveButton("Yes") { _, _ ->
                presenter.wipeData(activity!!)
                showToastMessage("All saved accounts and transactions are cleared!")
            }
            .setNegativeButton("No") { dialog, _ ->
                dialog.cancel()
            }
            .show()
    }

    private fun hasEmptyFields(): Boolean {
        if (editTextBaseUrl.text.isNullOrBlank()) {
            editTextBaseUrl.requestFocus()
            editTextBaseUrl.setError("BaseUrl is required")
            return true
        }

        return false
    }

    private fun setTextWatcher() {
        Selection.setSelection(editTextBaseUrl.text, editTextBaseUrl.text!!.length)
        editTextBaseUrl.addTextChangedListener(object : TextWatcher {

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            }

            override fun afterTextChanged(s: Editable) {
//                if (!s.toString().startsWith("http://")) {
//                    editTextBaseUrl.setText("http://")
//                    Selection.setSelection(editTextBaseUrl.text, editTextBaseUrl.text!!.length)
//                }

                if (s.toString().contains("\\s".toRegex())) {
                    val newText = editTextBaseUrl.text.toString().replace("\\s".toRegex(), "")
                    editTextBaseUrl.setText(newText)
                    Selection.setSelection(editTextBaseUrl.text, editTextBaseUrl.text!!.length)
                }
            }
        })
    }

    private fun isValidUrl(): Boolean {
        if (editTextBaseUrl.text.toString().startsWith("http://") &&
            editTextBaseUrl.text.toString().length > 7) {
            return true
        }
        return false
    }
}
