package com.tirsolutions.interpolbadgescanner.presentation.barcodescanner

import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcode

class BarcodePresenter : BarcodeContract.Presenter {

    private var view: BarcodeContract.View? = null

    override fun getBarcode(barcode: FirebaseVisionBarcode) {
        val barcodeString: String? = barcode.rawValue
        if (barcodeString != null) {
            if (barcodeString.length == 28 && barcodeString.startsWith("V")) {
                view!!.showVerifiedFragment(barcodeString)
            } else {
                view!!.showToastMessage("Invalid QR code")
            }
        } else {
            view!!.showToastMessage("Invalid QR code")
        }
    }

    override fun attachView(view: BarcodeContract.View) {
        this.view = view
    }

    override fun detachView() {
        this.view = null
    }
}
