package com.tirsolutions.interpolbadgescanner.presentation.success

import android.support.v4.app.FragmentActivity
import com.tirsolutions.interpolbadgescanner.BasePresenter
import com.tirsolutions.interpolbadgescanner.data.ApiResponse
import com.tirsolutions.interpolbadgescanner.data.GenericApiResponse
import com.tirsolutions.interpolbadgescanner.data.Setting
import com.tirsolutions.interpolbadgescanner.data.User
import retrofit2.Response

interface SuccessContract {

    interface View {

        fun showToastMessage(message:String)

        fun backToPreviousFragment()

        fun showAlertDialog(message: String)
    }

    interface Presenter : BasePresenter<View> {

        fun onSuccess(response: Response<GenericApiResponse>)

        fun onError(throwable: Throwable)

        fun onClickOk()

        fun syncTransaction(visitorId: String, baseUrl: String)

        fun saveTransaction(visitorId: String)

        fun hasSavedSetting(): Boolean

        fun getSetting(): Setting

        fun logoutUser()
    }
}
