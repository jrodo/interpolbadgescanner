package com.tirsolutions.interpolbadgescanner.presentation.barcodescanner

import android.content.Context
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import android.widget.Toast
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcode
import com.tirsolutions.interpolbadgescanner.R
import com.tirsolutions.interpolbadgescanner.presentation.barcodescanner.utils.CameraSource
import com.tirsolutions.interpolbadgescanner.presentation.barcodescanner.utils.CameraSourcePreview
import com.tirsolutions.interpolbadgescanner.presentation.barcodescanner.utils.GraphicOverlay
import com.tirsolutions.interpolbadgescanner.presentation.success.SuccessFragment
import com.tirsolutions.interpolbadgescanner.utils.FRAGMENT_TAG_SUCCESS
import com.tirsolutions.interpolbadgescanner.utils.FragmentHelper
import kotlinx.android.synthetic.main.activity_main.*
import java.io.IOException
import java.util.ArrayList

class BarcodeScannerFragment: Fragment(), ActivityCompat.OnRequestPermissionsResultCallback,
    CompoundButton.OnCheckedChangeListener, BarcodeContract.View  {

    private var cameraSource: CameraSource? = null
    private var preview: CameraSourcePreview? = null
    private var graphicOverlay: GraphicOverlay? = null

    private val presenter = BarcodePresenter()

    private val requiredPermissions: Array<String>?
        get() {
            try {
                val info = activity!!.packageManager.getPackageInfo(activity!!.packageName, PackageManager.GET_PERMISSIONS)
                val ps = info.requestedPermissions
                if (ps != null && ps.size > 0) {
                    return ps
                } else {
                    return arrayOf<String>()
                }
            } catch (e: Exception) {
                return arrayOf<String>()
            }
        }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        presenter.attachView(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_barcodescanner, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        preview = view.findViewById(R.id.firePreview)
        if (preview == null) {
            Log.d("BarcodeScanner", "Preview is null")
        }
        graphicOverlay = view.findViewById(R.id.fireFaceOverlay)
        if (graphicOverlay == null) {
            Log.d("BarcodeScanner", "graphicOverlay is null")
        }

        if (allPermissionsGranted()) {
            createCameraSource()
        } else {
            getRuntimePermissions()
        }
    }

    override fun onResume() {
        super.onResume()
        Log.d("BarcodeScanner", "onResume")
        startCameraSource()
    }

    override fun onPause() {
        super.onPause()
        preview!!.stop()
    }

    override fun onDetach() {
        super.onDetach()
        presenter.detachView()
        if (cameraSource != null) {
            Thread {
                activity!!.runOnUiThread {
                    cameraSource!!.release()
                }
            }
        }
    }

    override fun onCheckedChanged(buttonView: CompoundButton, isChecked: Boolean) {
        if (cameraSource != null) {
            if (isChecked) {
                cameraSource!!.setFacing(CameraSource.CAMERA_FACING_FRONT)
            } else {
                cameraSource!!.setFacing(CameraSource.CAMERA_FACING_BACK)
            }
        }
        preview!!.stop()
        startCameraSource()

    }

    override fun showBarcode(barcode: FirebaseVisionBarcode) {
        showToastMessage(barcode.rawValue!!)
    }

    override fun showVerifiedFragment(barcode: String) {
        val bundle = Bundle()
        bundle.putString("visitorId", barcode)

        val successFragment = SuccessFragment()
        successFragment.arguments = bundle

        FragmentHelper.replaceFragment(activity!!, successFragment, FRAGMENT_TAG_SUCCESS)
    }

    override fun showToastMessage(message: String) {
        Toast.makeText(activity!!, message, Toast.LENGTH_SHORT).show()
    }

    private fun createCameraSource() {
        // If there's no existing cameraSource, create one.
        if (cameraSource == null) {
            cameraSource = CameraSource(activity!!, graphicOverlay!!)
        }

        cameraSource!!.setMachineLearningFrameProcessor(BarcodeScanningProcessor(presenter))
    }

    /**
     * Starts or restarts the camera source, if it exists. If the camera source doesn't exist yet
     * (e.g., because onResume was called before the camera source was created), this will be called
     * again when the camera source is created.
     */
    private fun startCameraSource() {
        if (cameraSource != null) {
            try {
                if (preview == null) {
                    Log.d("BarcodeScanner", "resume: Preview is null")
                }
                if (graphicOverlay == null) {
                    Log.d("BarcodeScanner", "resume: graphOverlay is null")
                }
                preview!!.start(cameraSource!!, graphicOverlay!!)
            } catch (e: IOException) {
                Log.e("BarcodeScanner", "Unable to start camera source.", e)
                cameraSource!!.release()
                cameraSource = null
            }
        }
    }

    private fun allPermissionsGranted(): Boolean {
        for (permission in requiredPermissions!!) {
            if (!isPermissionGranted(activity!!, permission)) {
                return false
            }
        }
        return true
    }

    private fun getRuntimePermissions() {
        val allNeededPermissions = ArrayList<String>()
        for (permission in requiredPermissions!!) {
            if (!isPermissionGranted(activity!!, permission)) {
                allNeededPermissions.add(permission)
            }
        }

        if (!allNeededPermissions.isEmpty()) {
            ActivityCompat.requestPermissions(
                activity!!, allNeededPermissions.toTypedArray(), PERMISSION_REQUESTS
            )
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (allPermissionsGranted()) {
            createCameraSource()
        }
    }

//    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
//        Log.i(TAG, "Permission granted!")
//        if (allPermissionsGranted()) {
//            createCameraSource()
//        }
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
//    }

    companion object {

        private val TAG = "LivePreviewActivity"
        private val PERMISSION_REQUESTS = 1

        private fun isPermissionGranted(context: Context, permission: String): Boolean {
            if (ContextCompat.checkSelfPermission(
                    context,
                    permission
                ) == PackageManager.PERMISSION_GRANTED
            ) {
                Log.i(TAG, "Permission granted: $permission")
                return true
            }
            Log.i(TAG, "Permission NOT granted: $permission")
            return false
        }
    }
}
