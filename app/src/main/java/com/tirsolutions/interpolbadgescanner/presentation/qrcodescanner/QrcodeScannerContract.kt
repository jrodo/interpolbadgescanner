package com.tirsolutions.interpolbadgescanner.presentation.qrcodescanner

import com.tirsolutions.interpolbadgescanner.BasePresenter
import com.tirsolutions.interpolbadgescanner.data.GenericApiResponse
import com.tirsolutions.interpolbadgescanner.data.User
import retrofit2.Response

interface QrcodeScannerContract {

    interface View {
        fun showBarcode(barcode: String)

        fun showVerifiedFragment(barcode: String)

        fun showInvalidFragment()

        fun showToastMessage(message: String)

        fun updateCounters()

        fun showProgressBar()

        fun hideProgressBar()

        fun showAlertDialog(message: String)
    }

    interface Presenter: BasePresenter<View> {

        fun getBarcode(barcode: String)

        fun getBadgeScanCount(): Int

        fun getBadgeSyncCount(): Int

        fun getLoggedInUser(): User

        fun logoutUser()

        fun syncTransactions()

        fun onSuccess(response: Response<GenericApiResponse>)

        fun onError(throwable: Throwable)
    }
}
