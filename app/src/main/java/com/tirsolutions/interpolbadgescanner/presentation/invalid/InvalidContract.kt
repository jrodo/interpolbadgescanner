package com.tirsolutions.interpolbadgescanner.presentation.invalid

import android.support.v4.app.FragmentActivity
import com.tirsolutions.interpolbadgescanner.BasePresenter

interface InvalidContract {

    interface View {

        fun showToastMessage(message:String)

        fun backToPreviousFragment()

        fun showAlertDialog(message: String)
    }

    interface Presenter : BasePresenter<View> {

        fun onClickOk()

        fun logoutUser()
    }
}
