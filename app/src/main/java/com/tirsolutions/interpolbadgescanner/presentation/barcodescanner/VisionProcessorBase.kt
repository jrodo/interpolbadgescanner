package com.tirsolutions.interpolbadgescanner.presentation.barcodescanner

import android.graphics.Bitmap
import android.support.annotation.GuardedBy
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.OnSuccessListener
import com.google.android.gms.tasks.Task
import com.google.firebase.ml.vision.common.FirebaseVisionImage
import com.google.firebase.ml.vision.common.FirebaseVisionImageMetadata
import com.tirsolutions.interpolbadgescanner.presentation.barcodescanner.utils.BitmapUtils
import com.tirsolutions.interpolbadgescanner.presentation.barcodescanner.utils.FrameMetadata
import com.tirsolutions.interpolbadgescanner.presentation.barcodescanner.utils.GraphicOverlay
import com.tirsolutions.interpolbadgescanner.presentation.barcodescanner.utils.VisionImageProcessor
import java.nio.ByteBuffer

abstract class VisionProcessorBase<T>: VisionImageProcessor {

    // To keep the latest images and its metadata.
    @GuardedBy("this")
    private var latestImage: ByteBuffer? = null

    @GuardedBy("this")
    private var latestImageMetaData: FrameMetadata? = null

    // To keep the images and metadata in process.
    @GuardedBy("this")
    private var processingImage: ByteBuffer? = null

    @GuardedBy("this")
    private var processingMetaData: FrameMetadata? = null

    override fun process(data: ByteBuffer, frameMetadata: FrameMetadata, graphicOverlay: GraphicOverlay) {
        latestImage = data
        latestImageMetaData = frameMetadata
        if (processingImage == null && processingMetaData == null) {
            processLatestImage(graphicOverlay)
        }
    }

    override fun process(bitmap: Bitmap, graphicOverlay: GraphicOverlay) {
        detectInVisionImage(
            null, FirebaseVisionImage.fromBitmap(bitmap), null, graphicOverlay
        )
    }

    override fun stop() {
    }

    private fun detectInVisionImage(originalCameraImage: Bitmap?, image: FirebaseVisionImage, metadata: FrameMetadata?, graphicOverlay: GraphicOverlay) {
        detectInImage(image).addOnSuccessListener(object: OnSuccessListener<T> {
            override fun onSuccess(results: T) {
                this@VisionProcessorBase.onSuccess(
                    originalCameraImage, results,
                    metadata!!,
                    graphicOverlay
                )
                processLatestImage(graphicOverlay)
            }
        }).addOnFailureListener(object: OnFailureListener {
            override fun onFailure(e: java.lang.Exception) {
                this@VisionProcessorBase.onFailure(e)
            }
        })
    }

    @Synchronized
    private fun processLatestImage(graphicOverlay: GraphicOverlay) {
        processingImage = latestImage
        processingMetaData = latestImageMetaData
        latestImage = null
        latestImageMetaData = null
        if (processingImage != null && processingMetaData != null) {
            processImage(processingImage!!, processingMetaData!!, graphicOverlay)
        }
    }

    private fun processImage(data: ByteBuffer, frameMetadata: FrameMetadata, graphicOverlay: GraphicOverlay) {
        val metadata = FirebaseVisionImageMetadata.Builder()
            .setFormat(FirebaseVisionImageMetadata.IMAGE_FORMAT_NV21)
            .setWidth(frameMetadata.width)
            .setHeight(frameMetadata.height)
            .setRotation(frameMetadata.rotation)
            .build()

        val bitmap = BitmapUtils.getBitmap(data, frameMetadata)
        detectInVisionImage(bitmap!!, FirebaseVisionImage.fromByteBuffer(data, metadata), frameMetadata, graphicOverlay)
    }

    protected abstract fun detectInImage(image: FirebaseVisionImage): Task<T>

    /**
     * Callback that executes with a successful detection result.
     *
     * @param originalCameraImage hold the original image from camera, used to draw the background
     * image.
     */
    protected abstract fun onSuccess(
        originalCameraImage: Bitmap?,
        barcodes: T,
        frameMetadata: FrameMetadata,
        graphicOverlay: GraphicOverlay
    )

    protected abstract fun onFailure(e: Exception)
}
