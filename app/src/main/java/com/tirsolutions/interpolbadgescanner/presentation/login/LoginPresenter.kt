package com.tirsolutions.interpolbadgescanner.presentation.login

import com.tirsolutions.interpolbadgescanner.data.ApiResponse
import com.tirsolutions.interpolbadgescanner.data.Setting
import com.tirsolutions.interpolbadgescanner.data.User
import com.tirsolutions.interpolbadgescanner.data.source.local.SettingDL
import com.tirsolutions.interpolbadgescanner.data.source.local.UserDL
import com.tirsolutions.interpolbadgescanner.data.source.remote.InterpolBadgeScannerApi
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import org.json.JSONObject
import retrofit2.Response

class LoginPresenter: LoginContract.Presenter {

    private var view: LoginContract.View? = null
    private var compositeDisposable: CompositeDisposable? = null
    private var interpolBadgeScannerApi = InterpolBadgeScannerApi()

    private var username: String? = null
    private var password: String? = null

    override fun onSuccess(response: Response<ApiResponse<User>>) {
//        view!!.showToastMessage(response.code().toString() + ": " + response.body())

        if (response.code() == 200){
            val user: User = response.body()!!.results!!
//            view!!.showToastMessage(response.body().toString())
            if (checkLoggedUser(this.username!!, this.password!!)) {
                updateLoggedUser(this.username!!, this.password!!)
            } else {
                this.saveUser(
                    User(
                        firstName = user.firstName,
                        lastName = user.lastName,
                        username = this.username!!,
                        password = this.password!!,
                        isLoggedIn = true
                    )
                )
            }

            view!!.showHomeFragment()
        } else {
            checkOfflineUser()

            val errorResponse = response.errorBody()!!.string()
            val jsonObject = JSONObject(errorResponse)

            view!!.hideProgressDialog()
            view!!.showToastMessage(response.code().toString() + ": " + jsonObject.get("message"))
        }
    }

    override fun onError(throwable: Throwable) {
        checkOfflineUser()

        view!!.hideProgressDialog()
        view!!.showToastMessage(throwable.message!!)
    }

    override fun getLoggedUser(): User {
        return UserDL.getInstance().getLast()
    }

    override fun onLoginClick(baseUrl: String, username: String, password: String) {
        view!!.showProgressDialog()

        compositeDisposable!!.add(
            interpolBadgeScannerApi.create(baseUrl)
                .login(username, password)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::onSuccess, this::onError)
        )

        this.username = username
        this.password = password
    }

    override fun saveUser(user: User) {
        UserDL.getInstance().save(user)
    }

    override fun isUserEmpty(): Boolean {
        return UserDL.getInstance().getAll().isEmpty()
    }

    override fun checkLoggedUser(username: String, password: String): Boolean {
        val loggedUser = UserDL.getInstance().getByCredentials(username, password)
        return loggedUser != null
    }

    override fun hasSavedSetting(): Boolean {
        return !SettingDL.getInstance().isEmpty()
    }

    override fun getSetting(): Setting {
        return SettingDL.getInstance().getLast()
    }

    override fun updateLoggedUser(username: String, password: String) {
        val loggedUser = UserDL.getInstance().getByCredentials(username, password)!!.id
        UserDL.getInstance().updateUserIsLoggedIn(true, loggedUser!!)
    }

    override fun clearLoggedInUser() {
        val users = UserDL.getInstance().getAll()
        if (users.isNotEmpty()) {
            users.forEach { user ->
                UserDL.getInstance().updateUserIsLoggedIn(false, user.id!!)
            }
        }
    }

    override fun getUser(): User? {
        return UserDL.getInstance().get(1)
    }

    override fun attachView(view: LoginContract.View) {
        this.view = view
        this.compositeDisposable = CompositeDisposable()
    }

    override fun detachView() {
        this.view = null
        if (this.compositeDisposable != null) this.compositeDisposable?.clear()
    }

    private fun checkOfflineUser() {
        if (!this.isUserEmpty()) {
            if (this.checkLoggedUser(this.username!!, this.password!!)) {
                val loggedUser = UserDL.getInstance().getByCredentials(this.username!!, this.password!!)!!.id
                UserDL.getInstance().updateUserIsLoggedIn(true, loggedUser!!)
                view!!.showToastMessage("Logged in offline.")
                view!!.showHomeFragment()
            } else {
                view!!.showToastMessage("User does not exist.")
            }
            return
        }
    }
}
