package com.tirsolutions.interpolbadgescanner.presentation.barcodescanner

import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF

import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcode
import com.tirsolutions.interpolbadgescanner.presentation.barcodescanner.utils.GraphicOverlay

/** Graphic instance for rendering Barcode position and content information in an overlay view.  */
class BarcodeGraphic internal constructor(
    overlay: GraphicOverlay,
    private val barcode: FirebaseVisionBarcode?
) : GraphicOverlay.Graphic(overlay) {

    private val rectPaint: Paint
    private val barcodePaint: Paint

    init {

        rectPaint = Paint()
        rectPaint.color = TEXT_COLOR
        rectPaint.style = Paint.Style.STROKE
        rectPaint.strokeWidth =
            STROKE_WIDTH

        barcodePaint = Paint()
        barcodePaint.color =
            TEXT_COLOR
        barcodePaint.textSize =
            TEXT_SIZE
    }

    /**
     * Draws the barcode block annotations for position, size, and raw value on the supplied canvas.
     */
    override fun draw(canvas: Canvas) {
        if (barcode == null) {
            throw IllegalStateException("Attempting to draw a null barcode.")
        }

        // Draws the bounding box around the BarcodeBlock.
        val rect = RectF(barcode.boundingBox)
        rect.left = translateX(rect.left)
        rect.top = translateY(rect.top)
        rect.right = translateX(rect.right)
        rect.bottom = translateY(rect.bottom)
        canvas.drawRect(rect, rectPaint)

        // Renders the barcode at the bottom of the box.
        //        canvas.drawText(barcode.getRawValue(), rect.left, rect.bottom, barcodePaint);
    }

    companion object {

        private val TEXT_COLOR = Color.WHITE
        private val TEXT_SIZE = 54.0f
        private val STROKE_WIDTH = 4.0f
    }
}
