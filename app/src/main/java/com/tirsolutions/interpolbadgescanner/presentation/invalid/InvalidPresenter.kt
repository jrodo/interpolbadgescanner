package com.tirsolutions.interpolbadgescanner.presentation.invalid

import android.support.v4.app.FragmentActivity
import com.tirsolutions.interpolbadgescanner.data.source.local.UserDL

class InvalidPresenter: InvalidContract.Presenter {

    private var view: InvalidContract.View? = null

    override fun onClickOk() {
        view!!.backToPreviousFragment()
    }

    override fun logoutUser() {
        val loggedInUser = UserDL.getInstance().getLoggedInUser(true).id
        UserDL.getInstance().updateUserIsLoggedIn(false, loggedInUser!!)
    }

    override fun attachView(view: InvalidContract.View) {
        this.view = view
    }

    override fun detachView() {
        this.view = null
    }
}
