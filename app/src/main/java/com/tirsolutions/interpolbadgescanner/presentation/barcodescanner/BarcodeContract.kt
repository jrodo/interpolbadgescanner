package com.tirsolutions.interpolbadgescanner.presentation.barcodescanner

import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcode

interface BarcodeContract {

    interface View {
        fun showBarcode(barcode: FirebaseVisionBarcode)

        fun showVerifiedFragment(barcode: String)

        fun showToastMessage(message: String)
    }

    interface Presenter {
        fun getBarcode(barcode: FirebaseVisionBarcode)

        fun attachView(view: View)

        fun detachView()
    }
}
