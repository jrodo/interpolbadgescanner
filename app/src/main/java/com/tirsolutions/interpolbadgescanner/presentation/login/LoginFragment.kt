package com.tirsolutions.interpolbadgescanner.presentation.login

import android.content.Context
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.tirsolutions.interpolbadgescanner.ConnectivityChangeReceiver
import com.tirsolutions.interpolbadgescanner.R
import com.tirsolutions.interpolbadgescanner.presentation.barcodescanner.BarcodeScannerFragment
import com.tirsolutions.interpolbadgescanner.presentation.qrcodescanner.QrcodeScannerFragment
import com.tirsolutions.interpolbadgescanner.presentation.setting.SettingFragment
import com.tirsolutions.interpolbadgescanner.utils.*
import kotlinx.android.synthetic.main.fragment_login.*

class LoginFragment: Fragment(), LoginContract.View, ConnectivityChangeReceiver.ConnectivityReceiverListener {

    private val presenter = LoginPresenter()
    private val connectivityReceiver = ConnectivityChangeReceiver()
    private var isConnected = false
    private var baseUrl = String()
    private var clickCount = 0

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        presenter.attachView(this)
        presenter.clearLoggedInUser()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity!!.title = getString(R.string.app_name)

        if (presenter.hasSavedSetting()) {
            baseUrl = presenter.getSetting().baseUrl
        } else {
            baseUrl = BASE_URL
        }

        //TODO: remove this later
        if (presenter.getUser() != null) {
            editTextUsername.setText(presenter.getUser()!!.username)
            editTextPassword.setText(presenter.getUser()!!.password)
        }

        this.isConnected = ConnectivityHelper.isConnectedToNetwork(context!!)

        btnLogin.setOnClickListener {

            if (!hasEmptyFields()) {
                if (this.isConnected) {
                    presenter.onLoginClick(baseUrl, editTextUsername.text.toString(), editTextPassword.text.toString())
                } else {
                    if (!presenter.isUserEmpty()) {
                        if (presenter.checkLoggedUser(editTextUsername.text.toString(), editTextPassword.text.toString())) {
                            presenter.updateLoggedUser(editTextUsername.text.toString(), editTextPassword.text.toString())
                            showToastMessage("Logged in offline.")
                            showHomeFragment()
                        } else {
                            showToastMessage("User does not exist.")
                        }
                    } else {
                        showToastMessage("No saved account. Please connect to the network.")
                    }
                }
            }
        }

        appLogo.setOnClickListener {
            clickCount += 1

            if (clickCount == DEV_MODE_CLICK_COUNT) {
                FragmentHelper.replaceFragment(activity!!, SettingFragment(), FRAGMENT_TAG_SETTING)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        activity!!.registerReceiver(connectivityReceiver, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
        ConnectivityChangeReceiver.connectivityReceiverListener = this
    }

    override fun onPause() {
        super.onPause()
        activity!!.unregisterReceiver(connectivityReceiver)
    }

    override fun showProgressDialog() {
//        showToastMessage("Logging in... Please wait.")
        loginFields.visibility = View.GONE
        progressBar.visibility = View.VISIBLE
        appLogo.setOnClickListener(null)
    }

    override fun hideProgressDialog() {
        loginFields.visibility = View.VISIBLE
        progressBar.visibility = View.GONE

        appLogo.setOnClickListener {
            clickCount += 1

            if (clickCount == DEV_MODE_CLICK_COUNT) {
                FragmentHelper.replaceFragment(activity!!, SettingFragment(), FRAGMENT_TAG_SETTING)
            }
        }
    }

    override fun showToastMessage(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    override fun showHomeFragment() {
//        FragmentHelper.replaceFragment(activity!!, BarcodeScannerFragment(), FRAGMENT_TAG_BARCODESCANNER)
        FragmentHelper.replaceFragment(activity!!, QrcodeScannerFragment(), FRAGMENT_TAG_QRCODESCANNER)
    }

    override fun onDetach() {
        super.onDetach()
        presenter.detachView()
    }

    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        this.isConnected = ConnectivityHelper.isConnectedToNetwork(context!!)
        if (!isConnected) {
            showToastMessage("You are offline.")
        }
    }

    private fun hasEmptyFields(): Boolean {
        if (editTextUsername.text.isNullOrBlank()) {
            editTextUsername.requestFocus()
            editTextUsername.setError("Username is required")
            return true
        }

        if (editTextPassword.text.isNullOrBlank()) {
            editTextPassword.requestFocus()
            editTextPassword.setError("Password is required")
            return true
        }

        return false
    }
}
