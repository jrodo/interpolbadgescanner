package com.tirsolutions.interpolbadgescanner.presentation.qrcodescanner

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.util.Log
import android.view.*
import android.widget.Toast
import com.google.zxing.Result
import com.tirsolutions.interpolbadgescanner.R
import com.tirsolutions.interpolbadgescanner.presentation.barcodescanner.BarcodeScannerFragment
import com.tirsolutions.interpolbadgescanner.presentation.invalid.InvalidFragment
import com.tirsolutions.interpolbadgescanner.presentation.login.LoginFragment
import com.tirsolutions.interpolbadgescanner.presentation.success.SuccessFragment
import com.tirsolutions.interpolbadgescanner.utils.*
import kotlinx.android.synthetic.main.fragment_qrcodescanner.*
import me.dm7.barcodescanner.zxing.ZXingScannerView
import java.util.ArrayList



class QrcodeScannerFragment: Fragment(),QrcodeScannerContract.View, ZXingScannerView.ResultHandler,
    ActivityCompat.OnRequestPermissionsResultCallback{

    private val presenter = QrcodeScannerPresenter()

    private val requiredPermissions: Array<String>?
        get() {
            try {
                val info = activity!!.packageManager.getPackageInfo(activity!!.packageName, PackageManager.GET_PERMISSIONS)
                val ps = info.requestedPermissions
                if (ps != null && ps.size > 0) {
                    return ps
                } else {
                    return arrayOf<String>()
                }
            } catch (e: Exception) {
                return arrayOf<String>()
            }
        }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        presenter.attachView(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_qrcodescanner, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val totalBadgeCount = presenter.getBadgeScanCount()
        val totalBadgeSync = presenter.getBadgeSyncCount()
        txtBadgeScanned.text = getString(R.string.scan_count_text) + " $totalBadgeCount"
        txtBadgeSynced.text = getString(R.string.sync_count_text) + " $totalBadgeSync"

        activity!!.title = presenter.getLoggedInUser().username

        if (allPermissionsGranted()) {
            initializeZxingView()
        } else {
            getRuntimePermissions()
        }

        zxingView.setOnClickListener {
            initializeZxingView()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater!!.inflate(R.menu.sync_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            R.id.nav_logout -> {
                showAlertDialog("Are you sure you want to logout?")
            }
            R.id.nav_sync -> {
                presenter.syncTransactions()
            }
        }
        return false
    }

    override fun onResume() {
        super.onResume()
        zxingView!!.setResultHandler(this)
        zxingView!!.startCamera()
        zxingView!!.resumeCameraPreview(this)
    }

    override fun onPause() {
        super.onPause()
        zxingView!!.stopCamera()
    }

    override fun onDetach() {
        super.onDetach()
        presenter.detachView()
    }

    override fun handleResult(rawResult: Result?) {
//        showToastMessage(rawResult!!.text)
        presenter.getBarcode(rawResult!!.text)
        zxingView!!.resumeCameraPreview(this)
    }

    override fun showBarcode(barcode: String) {
        showToastMessage(barcode)
    }

    override fun showVerifiedFragment(barcode: String) {
        val bundle = Bundle()
        bundle.putString("visitorId", barcode)

        val successFragment = SuccessFragment()
        successFragment.arguments = bundle

        FragmentHelper.replaceFragment(activity!!, successFragment, FRAGMENT_TAG_SUCCESS)
    }

    override fun showInvalidFragment() {
        FragmentHelper.replaceFragment(activity!!, InvalidFragment(), FRAGMENT_TAG_INVALID)
    }

    override fun showToastMessage(message: String) {
        Toast.makeText(activity!!, message, Toast.LENGTH_SHORT).show()
    }

    override fun updateCounters() {
        activity!!.runOnUiThread {
            val totalBadgeCount = presenter.getBadgeScanCount()
            val totalBadgeSync = presenter.getBadgeSyncCount()
            txtBadgeScanned.text = getString(R.string.scan_count_text) + " $totalBadgeCount"
            txtBadgeSynced.text = getString(R.string.sync_count_text) + " $totalBadgeSync"
        }
    }

    override fun showProgressBar() {
        progressBar.visibility = View.VISIBLE
    }

    override fun hideProgressBar() {
        progressBar.visibility = View.GONE
    }

    override fun showAlertDialog(message: String) {
        val builder = AlertDialog.Builder(activity)
        builder.setMessage(message)
            .setCancelable(false)
            .setPositiveButton("Yes") { _, _ ->
                presenter.logoutUser()
                FragmentHelper.replaceFragment(activity!!, LoginFragment(), FRAGMENT_TAG_LOGIN)
            }
            .setNegativeButton("No") { dialog, _ ->
                dialog.cancel()
            }
            .show()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (allPermissionsGranted()) {
            initializeZxingView()
        }
    }

    private fun initializeZxingView() {
        zxingView!!.resumeCameraPreview(this)
        zxingView.setAutoFocus(true)
        zxingView.setAspectTolerance(0.5f)
    }

    private fun allPermissionsGranted(): Boolean {
        for (permission in requiredPermissions!!) {
            if (!isPermissionGranted(activity!!, permission)) {
                return false
            }
        }
        return true
    }

    private fun getRuntimePermissions() {
        val allNeededPermissions = ArrayList<String>()
        for (permission in requiredPermissions!!) {
            if (!isPermissionGranted(activity!!, permission)) {
                allNeededPermissions.add(permission)
            }
        }

        if (!allNeededPermissions.isEmpty()) {
            ActivityCompat.requestPermissions(
                activity!!, allNeededPermissions.toTypedArray(),
                PERMISSION_REQUESTS
            )
        }
    }

    companion object {

        private val TAG = "LivePreviewActivity"
        private val PERMISSION_REQUESTS = 1

        private fun isPermissionGranted(context: Context, permission: String): Boolean {
            if (ContextCompat.checkSelfPermission(
                    context,
                    permission
                ) == PackageManager.PERMISSION_GRANTED
            ) {
                Log.i(TAG, "Permission granted: $permission")
                return true
            }
            Log.i(TAG, "Permission NOT granted: $permission")
            return false
        }
    }
}
