package com.tirsolutions.interpolbadgescanner.presentation.setting

import android.content.Context
import com.tirsolutions.interpolbadgescanner.data.Setting
import com.tirsolutions.interpolbadgescanner.data.source.local.SettingDL
import com.tirsolutions.interpolbadgescanner.data.source.local.TransactionDL
import com.tirsolutions.interpolbadgescanner.data.source.local.UserDL
import com.tirsolutions.interpolbadgescanner.utils.*

class SettingPresenter: SettingContract.Presenter {

    private var view: SettingContract.View? = null

    override fun onSaveClick(baseUrl: String) {
        SettingDL.getInstance().save(
            Setting(
                id = 1,
                baseUrl = baseUrl
            )
        )
        view!!.showToastMessage("Settings saved.")
        view!!.showLoginFragment()
    }

    override fun hasSavedSetting(): Boolean {
        return !SettingDL.getInstance().isEmpty()
    }

    override fun getSetting(): Setting {
        return SettingDL.getInstance().getLast()
    }

    override fun wipeData(context: Context) {
        //TODO: fix this
        TransactionDL.getInstance().clear()
//        UserDL.getInstance().clear()

        //reset sqlite sequences
        DatabaseHelper(context, DATABASE_NAME, DATABASE_VERSION).resetPointer(
            TABLE_NAME_TRANSACTIONS)
//        DatabaseHelper(context, DATABASE_NAME, DATABASE_VERSION).resetPointer(
//            TABLE_NAME_USERS)
    }

    override fun attachView(view: SettingContract.View) {
        this.view = view
    }

    override fun detachView() {
        this.view = null
    }
}
