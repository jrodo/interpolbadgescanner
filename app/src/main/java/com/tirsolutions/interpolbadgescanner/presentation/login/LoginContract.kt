package com.tirsolutions.interpolbadgescanner.presentation.login

import com.tirsolutions.interpolbadgescanner.BasePresenter
import com.tirsolutions.interpolbadgescanner.data.ApiResponse
import com.tirsolutions.interpolbadgescanner.data.Setting
import com.tirsolutions.interpolbadgescanner.data.User
import retrofit2.Response

interface LoginContract {

    interface View {

        fun showProgressDialog()

        fun hideProgressDialog()

        fun showToastMessage(message:String)

        fun showHomeFragment()
    }

    interface Presenter: BasePresenter<View> {

        fun onSuccess(response: Response<ApiResponse<User>>)

        fun onError(throwable: Throwable)

        fun getLoggedUser(): User

        fun onLoginClick(baseUrl: String, username: String, password: String)

        fun saveUser(user: User)

        fun isUserEmpty(): Boolean

        fun checkLoggedUser(username: String, password: String): Boolean

        fun hasSavedSetting(): Boolean

        fun getSetting(): Setting

        fun updateLoggedUser(username: String, password: String)

        fun clearLoggedInUser()

        fun getUser(): User?
    }
}
