package com.tirsolutions.interpolbadgescanner.presentation.setting

import android.content.Context
import com.tirsolutions.interpolbadgescanner.BasePresenter
import com.tirsolutions.interpolbadgescanner.data.Setting

interface SettingContract {

    interface View {

        fun showProgressDialog()

        fun hideProgressDialog()

        fun showToastMessage(message:String)

        fun showLoginFragment()

        fun showAlertDialog(message: String)
    }

    interface Presenter: BasePresenter<View> {

        fun onSaveClick(baseUrl: String)

        fun hasSavedSetting(): Boolean

        fun getSetting(): Setting

        fun wipeData(context: Context)
    }
}
