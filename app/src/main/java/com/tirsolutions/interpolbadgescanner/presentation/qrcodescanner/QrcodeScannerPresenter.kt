package com.tirsolutions.interpolbadgescanner.presentation.qrcodescanner

import com.tirsolutions.interpolbadgescanner.data.GenericApiResponse
import com.tirsolutions.interpolbadgescanner.data.Transaction
import com.tirsolutions.interpolbadgescanner.data.User
import com.tirsolutions.interpolbadgescanner.data.source.local.SettingDL
import com.tirsolutions.interpolbadgescanner.data.source.local.TransactionDL
import com.tirsolutions.interpolbadgescanner.data.source.local.UserDL
import com.tirsolutions.interpolbadgescanner.data.source.remote.InterpolBadgeScannerApi
import com.tirsolutions.interpolbadgescanner.utils.BASE_URL
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import org.json.JSONObject
import retrofit2.Response
import java.lang.Exception

class QrcodeScannerPresenter: QrcodeScannerContract.Presenter {

    private var view: QrcodeScannerContract.View? = null
    private var compositeDisposable: CompositeDisposable? = null
    private var interpolBadgeScannerApi = InterpolBadgeScannerApi()

    private var localTransactions: List<Transaction>? = null

    override fun getBarcode(barcode: String) {
        if (barcode.length == 28 && barcode.startsWith("V")) {
            view!!.showVerifiedFragment(barcode)
        } else {
            view!!.showInvalidFragment()
//            view!!.showToastMessage("Invalid QR code")
        }
    }

    override fun getBadgeScanCount(): Int {
        val user = UserDL.getInstance().getLoggedInUser(true).id
        return TransactionDL.getInstance().getAllTransactionByUser(user!!).size
    }

    override fun getBadgeSyncCount(): Int {
        val user = UserDL.getInstance().getLoggedInUser(true).id
        return TransactionDL.getInstance().getAllLocalTransactionByUser(true, user!!).size
    }

    override fun getLoggedInUser(): User {
        return UserDL.getInstance().getLoggedInUser(true)
    }

    override fun logoutUser() {
        val loggedInUser = UserDL.getInstance().getLoggedInUser(true).id
        UserDL.getInstance().updateUserIsLoggedIn(false, loggedInUser!!)
    }

    override fun syncTransactions() {
        val user = UserDL.getInstance().getLoggedInUser(true).id
        localTransactions = TransactionDL.getInstance().getAllLocalTransactionByUser(false, user!!)

        if (localTransactions!!.isEmpty()) {
            view!!.showToastMessage("No transactions to sync.")
        } else {
            view!!.showProgressBar()
            view!!.showToastMessage("Syncing transaction logs...")

            var baseUrl = String()

            if (SettingDL.getInstance().isEmpty()) {
                baseUrl = BASE_URL
            } else {
                baseUrl = SettingDL.getInstance().getLast().baseUrl
            }


            compositeDisposable!!.add(
                interpolBadgeScannerApi.create(baseUrl)
                    .transactionBulk(ArrayList(localTransactions))
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(this::onSuccess, this::onError)
            )
        }
    }

    override fun onSuccess(response: Response<GenericApiResponse>) {
        view!!.hideProgressBar()
        if (response.code() == 201){
            localTransactions!!.forEach { transaction ->
                TransactionDL.getInstance().updateIsUploaded(true, transaction.id!!)
            }
            view!!.showToastMessage("Transaction synced to server.")
            view!!.updateCounters()

        } else {
            val errorResponse = response.errorBody()!!.string()
            val jsonObject = JSONObject(errorResponse)
            view!!.showToastMessage("Server error.")
//            view!!.showToastMessage(response.code().toString() + ": " + jsonObject.get("message"))
        }
    }

    override fun onError(throwable: Throwable) {
        view!!.hideProgressBar()
        try {
            view!!.showToastMessage("Server did not respond.")
        } catch (e: Exception) {
//            view!!.showToastMessage("Error API response")
        }
    }

    override fun attachView(view: QrcodeScannerContract.View) {
        this.view = view
        this.compositeDisposable = CompositeDisposable()
    }

    override fun detachView() {
        this.view = null
        if (this.compositeDisposable != null) this.compositeDisposable?.clear()
    }
}
