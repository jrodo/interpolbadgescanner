package com.tirsolutions.interpolbadgescanner.presentation.success

import android.support.v4.app.FragmentActivity
import com.tirsolutions.interpolbadgescanner.data.GenericApiResponse
import com.tirsolutions.interpolbadgescanner.data.Setting
import com.tirsolutions.interpolbadgescanner.data.Transaction
import com.tirsolutions.interpolbadgescanner.data.source.local.SettingDL
import com.tirsolutions.interpolbadgescanner.data.source.local.TransactionDL
import com.tirsolutions.interpolbadgescanner.data.source.local.UserDL
import com.tirsolutions.interpolbadgescanner.data.source.remote.InterpolBadgeScannerApi
import com.tirsolutions.interpolbadgescanner.presentation.barcodescanner.BarcodeScannerFragment
import com.tirsolutions.interpolbadgescanner.presentation.qrcodescanner.QrcodeScannerFragment
import com.tirsolutions.interpolbadgescanner.utils.FRAGMENT_TAG_BARCODESCANNER
import com.tirsolutions.interpolbadgescanner.utils.FRAGMENT_TAG_QRCODESCANNER
import com.tirsolutions.interpolbadgescanner.utils.FragmentHelper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import org.json.JSONObject
import retrofit2.Response
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Locale

class SuccessPresenter: SuccessContract.Presenter {

    private var view: SuccessContract.View? = null
    private var compositeDisposable: CompositeDisposable? = null
    private var interpolBadgeScannerApi = InterpolBadgeScannerApi()

    private var localTransactions: List<Transaction>? = null

    override fun onSuccess(response: Response<GenericApiResponse>) {
        if (response.code() == 201){
            view!!.showToastMessage("Transaction synced to server.")
            localTransactions!!.forEach { transaction ->
                TransactionDL.getInstance().updateIsUploaded(true, transaction.id!!)
            }

        } else {
            val errorResponse = response.errorBody()!!.string()
            val jsonObject = JSONObject(errorResponse)
            view!!.showToastMessage("Server error. Transaction saved offline.")
//            view!!.showToastMessage(response.code().toString() + ": " + jsonObject.get("message"))
        }
    }

    override fun onError(throwable: Throwable) {
        try {
            view!!.showToastMessage("Server did not respond. Transaction saved offline.")
        } catch (e: Exception) {
//            view!!.showToastMessage("Error API response")
        }
    }

    override fun onClickOk() {
        view!!.backToPreviousFragment()
    }

    override fun syncTransaction(visitorId: String, baseUrl: String) {
        val user = UserDL.getInstance().getLoggedInUser(true).id
        localTransactions = TransactionDL.getInstance().getAllLocalTransactionByUser(false, user!!)

        compositeDisposable!!.add(
            interpolBadgeScannerApi.create(baseUrl)
                .transactionBulk(ArrayList(localTransactions))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::onSuccess, this::onError)
        )
    }

    override fun saveTransaction(visitorId: String) {
        val currentTime = getCurrentDateTime()
        val loggedInUser = UserDL.getInstance().getLoggedInUser(true).id
        val data = Transaction(
            visitorId = visitorId,
            logType = "IN",
            createdAt = currentTime,
            updatedAt = currentTime,
            isUploaded = false,
            userId = loggedInUser!!
        )
        TransactionDL.getInstance().save(data)
    }

    override fun hasSavedSetting(): Boolean {
        return !SettingDL.getInstance().isEmpty()
    }

    override fun getSetting(): Setting {
        return SettingDL.getInstance().getLast()
    }

    override fun logoutUser() {
        val loggedInUser = UserDL.getInstance().getLoggedInUser(true).id
        UserDL.getInstance().updateUserIsLoggedIn(false, loggedInUser!!)
    }

    override fun attachView(view: SuccessContract.View) {
        this.view = view
        this.compositeDisposable = CompositeDisposable()
    }

    override fun detachView() {
        this.view = null
        if (this.compositeDisposable != null) this.compositeDisposable?.clear()
    }

    private fun getCurrentDateTime(): String {
        val calendar = Calendar.getInstance()
        val date = calendar.time
        val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSSSS", Locale.getDefault())
        return sdf.format(date)
    }
}
