package com.tirsolutions.interpolbadgescanner.presentation.success

import android.app.AlertDialog
import android.content.Context
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.*
import android.widget.Toast
import com.tirsolutions.interpolbadgescanner.ConnectivityChangeReceiver
import com.tirsolutions.interpolbadgescanner.R
import com.tirsolutions.interpolbadgescanner.presentation.login.LoginFragment
import com.tirsolutions.interpolbadgescanner.presentation.qrcodescanner.QrcodeScannerFragment
import com.tirsolutions.interpolbadgescanner.utils.*
import kotlinx.android.synthetic.main.fragment_success.*

class SuccessFragment : Fragment(), SuccessContract.View, ConnectivityChangeReceiver.ConnectivityReceiverListener{

    private val presenter = SuccessPresenter()
    private var visitorId = String()
    private val connectivityReceiver = ConnectivityChangeReceiver()
    private var isConnected = false
    private var baseUrl = String()

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        presenter.attachView(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (arguments != null){
            visitorId = arguments!!.getString("visitorId")!!
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_success, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        this.isConnected = ConnectivityHelper.isConnectedToNetwork(context!!)

        if (presenter.hasSavedSetting()) {
            baseUrl = presenter.getSetting().baseUrl
        } else {
            baseUrl = BASE_URL
        }

        presenter.saveTransaction(visitorId)

        if (this.isConnected) {
            presenter.syncTransaction(visitorId, baseUrl)
        } else {
            showToastMessage("Transaction saved offline.")
        }

        btnOk.setOnClickListener {
            presenter.onClickOk()
        }
    }

    override fun onResume() {
        super.onResume()
        activity!!.registerReceiver(connectivityReceiver, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
        ConnectivityChangeReceiver.connectivityReceiverListener = this
    }

    override fun onPause() {
        super.onPause()
        activity!!.unregisterReceiver(connectivityReceiver)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater!!.inflate(R.menu.app_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            R.id.nav_logout -> {
                showAlertDialog("Are you sure you want to logout?")
            }
        }
        return false
    }

    override fun onDetach() {
        super.onDetach()
        presenter.detachView()
    }

    override fun showToastMessage(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    override fun backToPreviousFragment() {
        FragmentHelper.replaceFragment(activity!!, QrcodeScannerFragment(), FRAGMENT_TAG_QRCODESCANNER)
    }

    override fun showAlertDialog(message: String) {
        val builder = AlertDialog.Builder(activity)
        builder.setMessage(message)
            .setCancelable(false)
            .setPositiveButton("Yes") { _, _ ->
                presenter.logoutUser()
                FragmentHelper.replaceFragment(activity!!, LoginFragment(), FRAGMENT_TAG_LOGIN)
            }
            .setNegativeButton("No") { dialog, _ ->
                dialog.cancel()
            }
            .show()
    }

    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        this.isConnected = ConnectivityHelper.isConnectedToNetwork(context!!)
        if (!isConnected) {
//            showToastMessage("You are offline.")
        }
    }
}
