package com.tirsolutions.interpolbadgescanner.presentation.invalid

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.*
import android.widget.Toast
import com.tirsolutions.interpolbadgescanner.R
import com.tirsolutions.interpolbadgescanner.presentation.login.LoginFragment
import com.tirsolutions.interpolbadgescanner.presentation.qrcodescanner.QrcodeScannerFragment
import com.tirsolutions.interpolbadgescanner.utils.FRAGMENT_TAG_LOGIN
import com.tirsolutions.interpolbadgescanner.utils.FRAGMENT_TAG_QRCODESCANNER
import com.tirsolutions.interpolbadgescanner.utils.FragmentHelper
import kotlinx.android.synthetic.main.fragment_success.*

class InvalidFragment: Fragment(), InvalidContract.View {

    private val presenter = InvalidPresenter()

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        presenter.attachView(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_invalid, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnOk.setOnClickListener {
            presenter.onClickOk()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater!!.inflate(R.menu.app_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            R.id.nav_logout -> {
                showAlertDialog("Are you sure you want to logout?")
            }
        }
        return false
    }

    override fun onDetach() {
        super.onDetach()
        presenter.detachView()
    }

    override fun showToastMessage(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    override fun backToPreviousFragment() {
        FragmentHelper.replaceFragment(activity!!, QrcodeScannerFragment(), FRAGMENT_TAG_QRCODESCANNER)
    }

    override fun showAlertDialog(message: String) {
        val builder = AlertDialog.Builder(activity)
        builder.setMessage(message)
            .setCancelable(false)
            .setPositiveButton("Yes") { _, _ ->
                presenter.logoutUser()
                FragmentHelper.replaceFragment(activity!!, LoginFragment(), FRAGMENT_TAG_LOGIN)
            }
            .setNegativeButton("No") { dialog, _ ->
                dialog.cancel()
            }
            .show()
    }
}
