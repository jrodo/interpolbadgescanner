package com.tirsolutions.interpolbadgescanner.utils

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DatabaseHelper (private val context: Context, private val dbName: String, private val dbVersion: Int):
    SQLiteOpenHelper(context, dbName, null, dbVersion) {

    override fun onCreate(db: SQLiteDatabase?) {}
    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {}

    fun resetPointer(tableName:String) {
        val database = DatabaseHelper(context, dbName, dbVersion)
        database.writableDatabase.execSQL("DELETE FROM sqlite_sequence WHERE name='$tableName';")
        database.close()
    }
}
