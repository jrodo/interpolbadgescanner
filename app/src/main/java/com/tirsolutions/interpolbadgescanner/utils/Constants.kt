package com.tirsolutions.interpolbadgescanner.utils

const val DATABASE_NAME = "interpolbadgescanner_db"
const val DATABASE_VERSION = 2

const val TABLE_NAME_USERS = "users"
const val TABLE_NAME_SETTINGS = "settings"
const val TABLE_NAME_TRANSACTIONS = "transactions"

const val FRAGMENT_TAG_LOGIN = "fragmentlogin"
const val FRAGMENT_TAG_BARCODESCANNER = "fragmentbarcodescanner"
const val FRAGMENT_TAG_QRCODESCANNER = "fragmentqrcodescanner"
const val FRAGMENT_TAG_SUCCESS = "fragmentsuccess"
const val FRAGMENT_TAG_INVALID = "fragmentinvalid"
const val FRAGMENT_TAG_SETTING = "fragmentsetting"
const val FRAGMENT_TAG_LOADING = "fragmentloading"


//const val BASE_URL = "http://192.168.4.82:8010" //server
//const val BASE_URL = "http://192.168.4.169:8000" //ob wifi
//const val BASE_URL = "http://192.168.43.75" //pixel
//const val BASE_URL = "http://devapi.interpol.oneberrysystem.com"
const val BASE_URL = "http://172.16.200.252:8010"
const val PATH_LOGIN = "/api/login/"
const val PATH_TRANSACTION = "/api/transaction/"
const val PATH_TRANSACTION_BULK = "/api/transaction-bulk/"


const val DEV_MODE_CLICK_COUNT = 6