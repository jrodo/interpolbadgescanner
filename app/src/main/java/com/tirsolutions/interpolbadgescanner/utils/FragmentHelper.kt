package com.tirsolutions.interpolbadgescanner.utils

import android.support.v4.app.DialogFragment
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import com.tirsolutions.interpolbadgescanner.R

class FragmentHelper {

    companion object {
        fun replaceFragment(activity: FragmentActivity, fragment: Fragment, tag: String){
            activity.supportFragmentManager
                .beginTransaction()
                .replace(R.id.container, fragment, tag)
                .commit()
        }

        fun addNonUiFragment(activity: FragmentActivity, fragment: Fragment, tag: String){
            activity.supportFragmentManager
                .beginTransaction()
                .add(R.id.containerNonUi, fragment, tag)
                .commit()
        }

        fun addDialogFragment(activity: FragmentActivity, dialogFragment: DialogFragment, tag: String){
            activity.supportFragmentManager
                .beginTransaction()
                .add(R.id.container, dialogFragment, tag)
                .commit()
        }
    }
}
